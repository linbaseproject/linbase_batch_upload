#!/usr/bin/python3
#Usage:
#python3 check_json.py /path/to/file.json

import json
import sys
import os

def parse(filename):
    try:
        with open(filename) as f:
            return json.load(f)
    except ValueError as e:
        print('invalid json: %s' % e)
        return None


if __name__ == "__main__":
    if parse(os.path.abspath(sys.argv[1])) is not None:
        print("This is a valid JSON file")


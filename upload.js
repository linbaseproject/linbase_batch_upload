var ARGV = process.argv;
const fs = require("fs");
const request = require('request');
const batch_size = 80;
const priority = 2;

const meta = JSON.parse(fs.readFileSync(ARGV[2], "utf8"));
const handleError = (i, e) => {
    console.log('Error occurred when submitting job ', i);
    console.log(e);
}

request.post({
    url: 'https://linbase.org/LINbase/index.php/api/auth',
	httpOptions: {timeout: 0},
    form: {
	    'id': 'USERNAME',
        'password': 'PASSWORD'
    }
}, (error, response, body) => {
    //console.log(response, body);
    if (error) {
	    console.log("Error occured during server connection\n", error);
	    return;
    }else{
    	res = JSON.parse(body);
	    if (res.status !== 0){
	        console.log(res.message);
	        return;
        }
    	console.log("Server connection successful with Session ID: ",res.session_id);
	// submit the first batch_size
    	submitBatch(0, res.session_id, priority);
    }	    
});

function valid_json(text){
    return (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
    replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
    replace(/(?:^|:|,)(?:\s*\[)+/g, '')));
}

function  escape_parenthesis(str) {
    return str.replace(/[()]/g, function(x){ return "\\".concat(x)});
}

// submits the requests 80 at a time and waits for the response
// of the last one before running the next batch
function submitBatch(counter, sessionId, priority){
    for (let i = counter; i < Math.min(meta.length, counter+batch_size); ++i) {
	try {
            let m = meta[i];
            let strain = m.taxonomy.strain;
            m.taxonomy = escape_parenthesis(encodeURIComponent(JSON.stringify(m.taxonomy)));
            m.attributes = escape_parenthesis(encodeURIComponent(JSON.stringify(m.attributes)));
            m.priority = JSON.stringify(priority);
            console.log('Job', i+1, 'Posted');
            request.post({
		    url: 'https://linbase.org/LINbase/index.php/api/jobs',
                form: {
                    session_id: sessionId,
                    name: 'upload',
                    title: 'Upload Genome ' + strain,
                    priority: JSON.stringify(priority),
                    args: encodeURIComponent(JSON.stringify(m))
                }
            }, (error, response, body) => {
		if ( i % batch_size === 0){
			// wait 30 secs to run the next iteration
		        setTimeout(function() { submitBatch(i+batch_size,sessionId, priority)}, 30000);
		}
                if (error) return handleError(i, error);
                // if result is a valid json
                if (valid_json(body)) {

                    let res = JSON.parse(body);
                    if (res.status !== 0) return handleError(i, res);
                    console.log('Job', i+1, 'successfully submitted.');

                }else{
                    console.log('Invalid JSON format. Job', i+1, 'aborted.');
                    console.log(body);
                }
            });
        } catch(e) {
            handleError(i, e);
        }
    }
	return 0;
}

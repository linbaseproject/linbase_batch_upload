#!/usr/bin/python3
#Usage:
#python3 linbase_csv_to_json.py /path/to/input.csv /path/to/output.json

import pandas as pd
import numpy as np
from itertools import groupby
from collections import OrderedDict
import json
import sys


def clean_dict(dictionary: dict):
    for key, value in list(dictionary.items()):
        if value is None or (isinstance(value, str) and value.strip() == ""):
            del dictionary[key]
        elif type(value) is dict:
            clean_dict(value)
    return dictionary


csv_file = sys.argv[1]
json_file = sys.argv[2]

input_fd = open(csv_file, encoding="utf-8", errors='backslashreplace')
df = pd.read_csv(input_fd, index_col = False, dtype=str)
available_columns = df.keys().tolist()

info_allowed_columns = [
    "Type strain", "NCBI Taxonomy ID", "NCBI Accession Number", "Date of isolation", "Country", "Region",
    "GPS Coordinates", "Link to peer-reviewed paper", "Host of isolation", "Secondary host", "Disease",
    "Symptom", "Phenotype", "Fluorescence", "Environmental source", "Source of isolation", "Outbreak",
    "Person/Institution who made the isolation", "R3B2 630/631 PCR test", "Sequencing Technology",
    "Sequencing Technology Version", "Assembly program", "Number of contigs", "Genome length after assembly, bp",
    "Coverage", "Alternate strain name", "Additional strain names"
]
taxa_allowed_columns = [
    "superkingdom", "phylum", "class", "order", "family", "genus", "species", "subspecies", "pathovar", "race",
    "biovar", "clade", "genomospecies", "phylogroup", "phylotype", "serotype", "serovar", "biotype", "sequevar",
    "strain"
]
# filename will be changed to uuid
required_columns = ['username', 'filename', 'interest', 'strain']

interest_allowed_values = [
    "Plant pathogens", "Environmental bacteria", "Uncultured bacteria",
    "Archaea", "Undefined interest", "Foodborne pathogens"
]


# if there are extra(incorrect header) columns
extra_columns = np.setdiff1d(
    np.array(available_columns),
    np.array(info_allowed_columns + taxa_allowed_columns + required_columns)
)
if extra_columns.size > 0:
    print("These columns were not recognized\n")
    print(extra_columns)
    exit(1)

missing_columns_bool = [el in available_columns for el in required_columns]
if not np.all(missing_columns_bool):
    print("These mandatory columns were missing\n")
    print(available_columns[[i for i, val in enumerate(required_columns) if not missing_columns_bool[i]]])
    exit(2)

wrong_interest_bool = [el in interest_allowed_values for el in df["interest"]]
if not np.all(wrong_interest_bool):
    print("Some interest values were not acceptable at the following rows\n")
    print([i+2 for i, val in enumerate(wrong_interest_bool) if not val])
    print("Acceptable values (Capitalization matters):")
    print(interest_allowed_values)
    exit(3)

# if there are empty mandatory fields
for col in ['username', 'filename', 'interest', 'strain']:
    empty_fields_bool = [pd.isna(el) for el in df[col]]
    if np.any(empty_fields_bool):
        print("The mandatory values in the \"{}\" column were missing at the following rows\n".format(col))
        print([i+2 for i, val in enumerate(empty_fields_bool) if val])
        exit(4)

# if the column is not unique
for col in ["filename"]:
    if len(df[col]) != len(np.unique(df[col])):
        print("You have two or more uploads using the same filename\n"
              "Please check the filename column for duplicate values")
        exit(5)

df["interest"] = df["interest"].map(lambda x: x.replace(" ", "%20"))
results = []
error_flag = False
df = df.fillna("")
df = df.replace({"'": ""}, regex=True)
df = df.applymap(lambda x: x.strip() if isinstance(x, str) else x)

for row in df.iterrows():
    row = row[1]
    info = row[np.intersect1d(info_allowed_columns, available_columns)]
    taxa = row[np.intersect1d(taxa_allowed_columns, available_columns)]
    results.append(OrderedDict([("username", row['username']),
                                ("uuid", row['filename']),
                                ("interest", row['interest']),
                                ("attributes", info.to_dict()),
                                ("taxonomy", taxa.to_dict())]))


if error_flag:
    print("You have some errors. Please fix them before trying again")
    exit(-1)

results = [clean_dict(row) for row in results]
with open(json_file, 'w') as json_f:
    json_f.write(json.dumps(results, indent=4))

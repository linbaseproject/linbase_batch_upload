# Batch Upload to [LINbase.org](https://linbase.org)

First you need elevated privileges to be able to batch upload to LINbase.
To get this access please email [LINbase@vt.edu](mailto:LINbase@vt.edu)
with your project description.

**There are four steps to successfully bach upload to LINbase**

## 1. Genome sequence data

Step requirements:
- Contact developers

Currently, there is no mechanism to remotely send the genome sequences when trying
to batch upload genomes. You could use the 
[Upload](https://linbase.org/LINbase/index.php/upload) page on LINbase to add your
genomes one-by-one. So you need to contact the developers to transfer the sequences.

## 2. Prepare the metadata for your genomes

Step requirements:
- Python3 + Pandas

You can use the sheet 
[linbase_batch_upload_example.xlsx](linbase_batch_upload_example.xlsx)
as a guide on how to
create a table of metadata. Please keep in mind all the included headers in the 
sheet are optional other than the first four (username, filename, interest, strain) 
which are mandatory (highlighted in red). Please use the **dropdown** when choosing
the interest of your genome.
The `filename` column should also be unique for each submission.
The other columns can be safely removed if not needed.

After your table is ready you should save your sheet as a `.csv` file and run:
```shell script
python3 linbase_csv_to_json.py /path/to/input.csv /path/to/output.json
```
This will generate a `.json` metadata file needed for the submission.

**You can try this out using the example file `test.csv`.**

## 3. Verify your JSON metadata

Step requirements:
- Python3

If you have used step 2 to create your JSON metadata and have not any changes
you can skip this step. Otherwise if you have built the JSON yourself or have
edited the file from step 2 you should verify the file to be safe.
```shell script
python3 check_json.py /path/to/file.json
```
and if successful you should get the following result if your JSON is
valid
```shell script
This is a valid JSON file
```

## 4. Submit your metadata

Step requirements:
- NodeJs + request module

You can install the request module using `npm install request`.

After you have the requirements you should replace `USERNAME` and `PASSWORD`
in `upload.js` based on your LINbase login credentials. Now you can make the submission.

**Linux**
```shell script
bash send.sh /path/to/metadata.json
```

**Windows**
```commandline
NODE_EXTRA_CA_CERTS="\full\path\to\GeoTrust_all.pem" node upload.js "\path\to\metadata.json"
``` 
The script will submit the metadata in batches of 80 which is the recommended
number currently to ensure stability. When successful you will get something
similar the following output:
```shell script
Server connection successful with Session ID:  29fc9b5cece008344af2f98xxxxxxxxxxxxxxxxx
Job 1 Posted
Job 2 Posted
...
```
and eventually recieve
```shell script
Job 1 successfully submitted.
Job 2 successfully submitted.
...
```

Afterwards the submissions will appear in your submission history at
[linbase.org](https://linbase.org)
in order of newest to oldest submission.